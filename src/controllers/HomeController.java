/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import javax.script.ScriptEngineManager;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import models.LeontiefClosed;
import models.LeontiefOpen;
import models.PriceDetermination;
import views.Home;

/**
 *
 * @author the Code
 */
public class HomeController {

    ScriptEngineManager s = new ScriptEngineManager();
    ScriptEngine engine = s.getEngineByName("JavaScript");

    private final Home home;
    private final PriceDetermination price;
    private final LeontiefOpen open;
    private final LeontiefClosed closed;

    /**
     * function to handle the connection between the view and the model
     * @param price
     * @param open
     * @param closed
     * @param home 
     */
    public HomeController(PriceDetermination price, LeontiefOpen open, LeontiefClosed closed, Home home) {
        this.home = home;
        this.price = price;
        this.open = open;
        this.closed = closed;

        home.getP11().requestFocusInWindow();

        //Price determination controllers
        //Actions for TextField
        home.getP11().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getP11().getActionMap().put("pressed", new P11());
        home.getP12().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getP12().getActionMap().put("pressed", new P12());
        home.getP13().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getP13().getActionMap().put("pressed", new P13());
        home.getP21().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getP21().getActionMap().put("pressed", new P21());
        home.getP22().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getP22().getActionMap().put("pressed", new P22());
        home.getP23().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getP23().getActionMap().put("pressed", new P23());
        home.getP31().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getP31().getActionMap().put("pressed", new P31());
        home.getP32().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getP32().getActionMap().put("pressed", new P32());
        home.getP33().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getP33().getActionMap().put("pressed", new P33());

        home.getTotalPrice1().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getTotalPrice1().getActionMap().put("pressed", new TotalPrice1());
        home.getTotalPrice2().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getTotalPrice2().getActionMap().put("pressed", new TotalPrice2());
        home.getTotalPrice3().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getTotalPrice3().getActionMap().put("pressed", new TotalPrice3());

        //Key listeners for textfields
        home.getP11().addKeyListener(new CheckValues());
        home.getP12().addKeyListener(new CheckValues());
        home.getP13().addKeyListener(new CheckValues());
        home.getP21().addKeyListener(new CheckValues());
        home.getP22().addKeyListener(new CheckValues());
        home.getP23().addKeyListener(new CheckValues());
        home.getP31().addKeyListener(new CheckValues());
        home.getP32().addKeyListener(new CheckValues());
        home.getP33().addKeyListener(new CheckValues());

        home.getTotalPrice1().addKeyListener(new CheckValues());
        home.getTotalPrice2().addKeyListener(new CheckValues());
        home.getTotalPrice3().addKeyListener(new CheckValues());

        home.getCalcPriceBtn().addActionListener(new CalcPrice());

        home.getCalcPriceBtn().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getCalcPriceBtn().getActionMap().put("pressed", new CalcPriceBtn());

        //Leontief Open Controllers
        home.getLO11().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLO11().getActionMap().put("pressed", new LO11());
        home.getLO12().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLO12().getActionMap().put("pressed", new LO12());
        home.getLO13().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLO13().getActionMap().put("pressed", new LO13());
        home.getLO21().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLO21().getActionMap().put("pressed", new LO21());
        home.getLO22().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLO22().getActionMap().put("pressed", new LO22());
        home.getLO23().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLO23().getActionMap().put("pressed", new LO23());
        home.getLO31().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLO31().getActionMap().put("pressed", new LO31());
        home.getLO32().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLO32().getActionMap().put("pressed", new LO32());
        home.getLO33().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLO33().getActionMap().put("pressed", new LO33());

        home.getLODemand1().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), new LODemand1());
        home.getLODemand1().getActionMap().put("pressed", new LODemand1());
        home.getLODemand2().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), new LODemand2());
        home.getLODemand2().getActionMap().put("pressed", new LODemand2());
        home.getLODemand3().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), new LODemand3());
        home.getLODemand3().getActionMap().put("pressed", new LODemand3());

        //Key listeners for textfields
        home.getLO11().addKeyListener(new CheckValues());
        home.getLO12().addKeyListener(new CheckValues());
        home.getLO13().addKeyListener(new CheckValues());
        home.getLO21().addKeyListener(new CheckValues());
        home.getLO22().addKeyListener(new CheckValues());
        home.getLO23().addKeyListener(new CheckValues());
        home.getLO31().addKeyListener(new CheckValues());
        home.getLO32().addKeyListener(new CheckValues());
        home.getLO33().addKeyListener(new CheckValues());

        home.getLODemand1().addKeyListener(new CheckValues());
        home.getLODemand2().addKeyListener(new CheckValues());
        home.getLODemand3().addKeyListener(new CheckValues());

        home.getCalcLeontiefOpenBtn().addActionListener(new CalcLeontiefOpen());
        home.getCalcLeontiefOpenBtn().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getCalcLeontiefOpenBtn().getActionMap().put("pressed", new CalcLeontiefOpenBtn());

        //Leontief closed controller
        home.getLC11().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLC11().getActionMap().put("pressed", new LC11());
        home.getLC12().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLC12().getActionMap().put("pressed", new LC12());
        home.getLC13().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLC13().getActionMap().put("pressed", new LC13());
        home.getLC21().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLC21().getActionMap().put("pressed", new LC21());
        home.getLC22().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLC22().getActionMap().put("pressed", new LC22());
        home.getLC23().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLC23().getActionMap().put("pressed", new LC23());
        home.getLC31().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLC31().getActionMap().put("pressed", new LC31());
        home.getLC32().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLC32().getActionMap().put("pressed", new LC32());
        home.getLC33().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getLC33().getActionMap().put("pressed", new LC33());

        home.getLCDemand1().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), new LCDemand1());
        home.getLCDemand1().getActionMap().put("pressed", new LCDemand1());
        home.getLCDemand2().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), new LCDemand2());
        home.getLCDemand2().getActionMap().put("pressed", new LCDemand2());
        home.getLCDemand3().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), new LCDemand3());
        home.getLCDemand3().getActionMap().put("pressed", new LCDemand3());

        //Key listeners for textfields
        home.getLC11().addKeyListener(new CheckValues());
        home.getLC12().addKeyListener(new CheckValues());
        home.getLC13().addKeyListener(new CheckValues());
        home.getLC21().addKeyListener(new CheckValues());
        home.getLC22().addKeyListener(new CheckValues());
        home.getLC23().addKeyListener(new CheckValues());
        home.getLC31().addKeyListener(new CheckValues());
        home.getLC32().addKeyListener(new CheckValues());
        home.getLC33().addKeyListener(new CheckValues());

        home.getLCDemand1().addKeyListener(new CheckValues());
        home.getLCDemand2().addKeyListener(new CheckValues());
        home.getLCDemand3().addKeyListener(new CheckValues());

        home.getCalcLeontiefClosedBtn().addActionListener(new CalcLeontiefClosed());
        home.getCalcLeontiefClosedBtn().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pressed");
        home.getCalcLeontiefClosedBtn().getActionMap().put("pressed", new CalcLeontiefClosedBtn());

    }

    //Check to ensure if all matrix values have been filled.
    class CalcPrice implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (home.getP11().getText().equals("")) {
                home.getP11().requestFocusInWindow();
            } else if (home.getP12().getText().equals("")) {
                home.getP12().requestFocusInWindow();
            } else if (home.getP13().getText().equals("")) {
                home.getP13().requestFocusInWindow();
            } else if (home.getP21().getText().equals("")) {
                home.getP21().requestFocusInWindow();
            } else if (home.getP22().getText().equals("")) {
                home.getP22().requestFocusInWindow();
            } else if (home.getP23().getText().equals("")) {
                home.getP23().requestFocusInWindow();
            } else if (home.getP31().getText().equals("")) {
                home.getP31().requestFocusInWindow();
            } else if (home.getP32().getText().equals("")) {
                home.getP32().requestFocusInWindow();
            } else if (home.getP33().getText().equals("")) {
                home.getP33().requestFocusInWindow();
            } else if (home.getTotalPrice1().getText().equals("")) {
                home.getTotalPrice1().requestFocusInWindow();
            } else if (home.getTotalPrice2().getText().equals("")) {
                home.getTotalPrice2().requestFocusInWindow();
            } else if (home.getTotalPrice3().getText().equals("")) {
                home.getTotalPrice3().requestFocusInWindow();
            } else {
                try {
                    price.InputValues(getPriceMatrix(), getPrice(), home.getPriceDeterminationResult());
                } catch (ScriptException ex) {
                }
            }
        }

    }

    //Check to ensure if all matrix values have been filled.
    class CalcPriceBtn extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (home.getP11().getText().equals("")) {
                home.getP11().requestFocusInWindow();
            } else if (home.getP12().getText().equals("")) {
                home.getP12().requestFocusInWindow();
            } else if (home.getP13().getText().equals("")) {
                home.getP13().requestFocusInWindow();
            } else if (home.getP21().getText().equals("")) {
                home.getP21().requestFocusInWindow();
            } else if (home.getP22().getText().equals("")) {
                home.getP22().requestFocusInWindow();
            } else if (home.getP23().getText().equals("")) {
                home.getP23().requestFocusInWindow();
            } else if (home.getP31().getText().equals("")) {
                home.getP31().requestFocusInWindow();
            } else if (home.getP32().getText().equals("")) {
                home.getP32().requestFocusInWindow();
            } else if (home.getP33().getText().equals("")) {
                home.getP33().requestFocusInWindow();
            } else if (home.getTotalPrice1().getText().equals("")) {
                home.getTotalPrice1().requestFocusInWindow();
            } else if (home.getTotalPrice2().getText().equals("")) {
                home.getTotalPrice2().requestFocusInWindow();
            } else if (home.getTotalPrice3().getText().equals("")) {
                home.getTotalPrice3().requestFocusInWindow();
            } else {
                try {
                    price.InputValues(getPriceMatrix(), getPrice(), home.getPriceDeterminationResult());
                } catch (ScriptException ex) {
                }
            }
        }

    }

    class P11 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getP12().requestFocusInWindow();
        }

    }

    class P12 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getP13().requestFocusInWindow();
        }

    }

    class P13 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getP21().requestFocusInWindow();
        }

    }

    class P21 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getP22().requestFocusInWindow();
        }

    }

    class P22 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getP23().requestFocusInWindow();
        }

    }

    class P23 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getP31().requestFocusInWindow();
        }

    }

    class P31 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getP32().requestFocusInWindow();
        }

    }

    class P32 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getP33().requestFocusInWindow();
        }

    }

    class P33 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getTotalPrice1().requestFocusInWindow();
        }

    }

    class TotalPrice1 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getTotalPrice2().requestFocusInWindow();
        }

    }

    class TotalPrice2 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getTotalPrice3().requestFocusInWindow();
        }

    }

    class TotalPrice3 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getCalcPriceBtn().requestFocusInWindow();
        }

    }

    //Actions For leontief controllers
    //Check to ensure if all matrix values have been filled.
    class CalcLeontiefOpen implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (home.getLO11().getText().equals("")) {
                home.getLO11().requestFocusInWindow();
            } else if (home.getLO12().getText().equals("")) {
                home.getLO12().requestFocusInWindow();
            } else if (home.getLO13().getText().equals("")) {
                home.getLO13().requestFocusInWindow();
            } else if (home.getLO21().getText().equals("")) {
                home.getLO21().requestFocusInWindow();
            } else if (home.getLO22().getText().equals("")) {
                home.getLO22().requestFocusInWindow();
            } else if (home.getLO23().getText().equals("")) {
                home.getLO23().requestFocusInWindow();
            } else if (home.getLO31().getText().equals("")) {
                home.getLO31().requestFocusInWindow();
            } else if (home.getLO32().getText().equals("")) {
                home.getLO32().requestFocusInWindow();
            } else if (home.getLO33().getText().equals("")) {
                home.getLO33().requestFocusInWindow();
            } else if (home.getLODemand1().getText().equals("")) {
                home.getLODemand1().requestFocusInWindow();
            } else if (home.getLODemand2().getText().equals("")) {
                home.getLODemand2().requestFocusInWindow();
            } else if (home.getLODemand3().getText().equals("")) {
                home.getLODemand3().requestFocusInWindow();
            } else {
                try {
                    open.InputValues(getOpenMatrix(), getDemand(), home.getLeontiefOpenResult());
                } catch (ScriptException ex) {
                }
            }
        }

    }

    //Check to ensure if all matrix values have been filled.
    class CalcLeontiefOpenBtn extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (home.getLO11().getText().equals("")) {
                home.getLO11().requestFocusInWindow();
            } else if (home.getLO12().getText().equals("")) {
                home.getLO12().requestFocusInWindow();
            } else if (home.getLO13().getText().equals("")) {
                home.getLO13().requestFocusInWindow();
            } else if (home.getLO21().getText().equals("")) {
                home.getLO21().requestFocusInWindow();
            } else if (home.getLO22().getText().equals("")) {
                home.getLO22().requestFocusInWindow();
            } else if (home.getLO23().getText().equals("")) {
                home.getLO23().requestFocusInWindow();
            } else if (home.getLO31().getText().equals("")) {
                home.getLO31().requestFocusInWindow();
            } else if (home.getLO32().getText().equals("")) {
                home.getLO32().requestFocusInWindow();
            } else if (home.getLO33().getText().equals("")) {
                home.getLO33().requestFocusInWindow();
            } else if (home.getLODemand1().getText().equals("")) {
                home.getLODemand1().requestFocusInWindow();
            } else if (home.getLODemand2().getText().equals("")) {
                home.getLODemand2().requestFocusInWindow();
            } else if (home.getLODemand3().getText().equals("")) {
                home.getLODemand3().requestFocusInWindow();
            } else {
                try {
                    open.InputValues(getOpenMatrix(), getDemand(), home.getLeontiefOpenResult());
                } catch (ScriptException ex) {
                }
            }
        }

    }

    class LO11 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLO12().requestFocusInWindow();
        }

    }

    class LO12 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLO13().requestFocusInWindow();
        }

    }

    class LO13 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLO21().requestFocusInWindow();
        }

    }

    class LO21 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLO22().requestFocusInWindow();
        }

    }

    class LO22 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLO23().requestFocusInWindow();
        }

    }

    class LO23 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLO31().requestFocusInWindow();
        }

    }

    class LO31 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLO32().requestFocusInWindow();
        }

    }

    class LO32 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLO33().requestFocusInWindow();
        }

    }

    class LO33 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLODemand1().requestFocusInWindow();
        }

    }

    class LODemand1 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLODemand2().requestFocusInWindow();
        }

    }

    class LODemand2 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLODemand3().requestFocusInWindow();
        }

    }

    class LODemand3 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getCalcLeontiefOpenBtn().requestFocusInWindow();
        }

    }

    //Actions for leontief closed controllers
    //Check to ensure if all matrix values have been filled.
    class CalcLeontiefClosed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (home.getLC11().getText().equals("")) {
                home.getLC11().requestFocusInWindow();
            } else if (home.getLC12().getText().equals("")) {
                home.getLC12().requestFocusInWindow();
            } else if (home.getLC13().getText().equals("")) {
                home.getLC13().requestFocusInWindow();
            } else if (home.getLC21().getText().equals("")) {
                home.getLC21().requestFocusInWindow();
            } else if (home.getLC22().getText().equals("")) {
                home.getLC22().requestFocusInWindow();
            } else if (home.getLC23().getText().equals("")) {
                home.getLC23().requestFocusInWindow();
            } else if (home.getLC31().getText().equals("")) {
                home.getLC31().requestFocusInWindow();
            } else if (home.getLC32().getText().equals("")) {
                home.getLC32().requestFocusInWindow();
            } else if (home.getLC33().getText().equals("")) {
                home.getLC33().requestFocusInWindow();
            } else {
                try {
                    closed.InputValues(getClosedMatrix(), getZeroDemand(), home.getLeontiefClosedResult());
                } catch (ScriptException ex) {
                }
            }
        }

    }

    //Check to ensure if all matrix values have been filled.
    class CalcLeontiefClosedBtn extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (home.getLC11().getText().equals("")) {
                home.getLC11().requestFocusInWindow();
            } else if (home.getLC12().getText().equals("")) {
                home.getLC12().requestFocusInWindow();
            } else if (home.getLC13().getText().equals("")) {
                home.getLC13().requestFocusInWindow();
            } else if (home.getLC21().getText().equals("")) {
                home.getLC21().requestFocusInWindow();
            } else if (home.getLC22().getText().equals("")) {
                home.getLC22().requestFocusInWindow();
            } else if (home.getLC23().getText().equals("")) {
                home.getLC23().requestFocusInWindow();
            } else if (home.getLC31().getText().equals("")) {
                home.getLC31().requestFocusInWindow();
            } else if (home.getLC32().getText().equals("")) {
                home.getLC32().requestFocusInWindow();
            } else if (home.getLC33().getText().equals("")) {
                home.getLC33().requestFocusInWindow();
            } else {
                try {
                    closed.InputValues(getClosedMatrix(), getZeroDemand(), home.getLeontiefClosedResult());
                } catch (ScriptException ex) {
                }
            }
        }

    }

    class LC11 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLC12().requestFocusInWindow();
        }

    }

    class LC12 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLC13().requestFocusInWindow();
        }

    }

    class LC13 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLC21().requestFocusInWindow();
        }

    }

    class LC21 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLC22().requestFocusInWindow();
        }

    }

    class LC22 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLC23().requestFocusInWindow();
        }

    }

    class LC23 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLC31().requestFocusInWindow();
        }

    }

    class LC31 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLC32().requestFocusInWindow();
        }

    }

    class LC32 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLC33().requestFocusInWindow();
        }

    }

    class LC33 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLCDemand1().requestFocusInWindow();
        }

    }

    class LCDemand1 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLCDemand2().requestFocusInWindow();
        }

    }

    class LCDemand2 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getLCDemand3().requestFocusInWindow();
        }

    }

    class LCDemand3 extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            home.getCalcLeontiefClosedBtn().requestFocusInWindow();
        }

    }

    //check To avoid illegal characters in textfields
    class CheckValues implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            char c = e.getKeyChar();
            if ((!(Character.isDigit(c)) && (c != '-') && (c != '.') && (c != '/')) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE)) {
                e.consume();
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }

    }

    public double[][] getPriceMatrix() throws ScriptException {

        double[][] A = new double[3][3];

        A[0][0] = (home.getP11().getText().contains("/")) ? (double) engine.eval(home.getP11().getText()) : Double.parseDouble(home.getP11().getText());
        A[0][1] = (home.getP12().getText().contains("/")) ? (double) engine.eval(home.getP12().getText()) : Double.parseDouble(home.getP12().getText());
        A[0][2] = (home.getP13().getText().contains("/")) ? (double) engine.eval(home.getP13().getText()) : Double.parseDouble(home.getP13().getText());
        A[1][0] = (home.getP21().getText().contains("/")) ? (double) engine.eval(home.getP21().getText()) : Double.parseDouble(home.getP21().getText());
        A[1][1] = (home.getP22().getText().contains("/")) ? (double) engine.eval(home.getP22().getText()) : Double.parseDouble(home.getP22().getText());
        A[1][2] = (home.getP23().getText().contains("/")) ? (double) engine.eval(home.getP23().getText()) : Double.parseDouble(home.getP23().getText());
        A[2][0] = (home.getP31().getText().contains("/")) ? (double) engine.eval(home.getP31().getText()) : Double.parseDouble(home.getP31().getText());
        A[2][1] = (home.getP32().getText().contains("/")) ? (double) engine.eval(home.getP32().getText()) : Double.parseDouble(home.getP32().getText());
        A[2][2] = (home.getP33().getText().contains("/")) ? (double) engine.eval(home.getP33().getText()) : Double.parseDouble(home.getP33().getText());

        return A;
    }

    public double[] getPrice() throws ScriptException {

        double[] B = new double[3];

        B[0] = (home.getTotalPrice1().getText().contains("/")) ? (double) engine.eval(home.getTotalPrice1().getText()) : Double.parseDouble(home.getTotalPrice1().getText());
        B[1] = (home.getTotalPrice2().getText().contains("/")) ? (double) engine.eval(home.getTotalPrice2().getText()) : Double.parseDouble(home.getTotalPrice2().getText());
        B[2] = (home.getTotalPrice3().getText().contains("/")) ? (double) engine.eval(home.getTotalPrice3().getText()) : Double.parseDouble(home.getTotalPrice3().getText());

        return B;
    }

    public double[][] getOpenMatrix() throws ScriptException {

        double[][] A = new double[3][3];

        A[0][0] = (home.getLO11().getText().contains("/")) ? (double) engine.eval(home.getLO11().getText()) : Double.parseDouble(home.getLO11().getText());
        A[0][1] = (home.getLO12().getText().contains("/")) ? (double) engine.eval(home.getLO12().getText()) : Double.parseDouble(home.getLO12().getText());
        A[0][2] = (home.getLO13().getText().contains("/")) ? (double) engine.eval(home.getLO13().getText()) : Double.parseDouble(home.getLO13().getText());
        A[1][0] = (home.getLO21().getText().contains("/")) ? (double) engine.eval(home.getLO21().getText()) : Double.parseDouble(home.getLO21().getText());
        A[1][1] = (home.getLO22().getText().contains("/")) ? (double) engine.eval(home.getLO22().getText()) : Double.parseDouble(home.getLO22().getText());
        A[1][2] = (home.getLO23().getText().contains("/")) ? (double) engine.eval(home.getLO23().getText()) : Double.parseDouble(home.getLO23().getText());
        A[2][0] = (home.getLO31().getText().contains("/")) ? (double) engine.eval(home.getLO31().getText()) : Double.parseDouble(home.getLO31().getText());
        A[2][1] = (home.getLO32().getText().contains("/")) ? (double) engine.eval(home.getLO32().getText()) : Double.parseDouble(home.getLO32().getText());
        A[2][2] = (home.getLO33().getText().contains("/")) ? (double) engine.eval(home.getLO33().getText()) : Double.parseDouble(home.getLO33().getText());

        return A;
    }

    public double[] getDemand() throws ScriptException {

        double[] B = new double[3];

        B[0] = (home.getLODemand1().getText().contains("/")) ? (double) engine.eval(home.getLODemand1().getText()) : Double.parseDouble(home.getLODemand1().getText());
        B[1] = (home.getLODemand2().getText().contains("/")) ? (double) engine.eval(home.getLODemand2().getText()) : Double.parseDouble(home.getLODemand2().getText());
        B[2] = (home.getLODemand3().getText().contains("/")) ? (double) engine.eval(home.getLODemand3().getText()) : Double.parseDouble(home.getLODemand3().getText());

        return B;
    }

    public double[][] getClosedMatrix() throws ScriptException {

        double[][] A = new double[3][3];

        A[0][0] = (home.getLC11().getText().contains("/")) ? (double) engine.eval(home.getLC11().getText()) : Double.parseDouble(home.getLC11().getText());
        A[0][1] = (home.getLC12().getText().contains("/")) ? (double) engine.eval(home.getLC12().getText()) : Double.parseDouble(home.getLC12().getText());
        A[0][2] = (home.getLC13().getText().contains("/")) ? (double) engine.eval(home.getLC13().getText()) : Double.parseDouble(home.getLC13().getText());
        A[1][0] = (home.getLC21().getText().contains("/")) ? (double) engine.eval(home.getLC21().getText()) : Double.parseDouble(home.getLC21().getText());
        A[1][1] = (home.getLC22().getText().contains("/")) ? (double) engine.eval(home.getLC22().getText()) : Double.parseDouble(home.getLC22().getText());
        A[1][2] = (home.getLC23().getText().contains("/")) ? (double) engine.eval(home.getLC23().getText()) : Double.parseDouble(home.getLC23().getText());
        A[2][0] = (home.getLC31().getText().contains("/")) ? (double) engine.eval(home.getLC31().getText()) : Double.parseDouble(home.getLC31().getText());
        A[2][1] = (home.getLC32().getText().contains("/")) ? (double) engine.eval(home.getLC32().getText()) : Double.parseDouble(home.getLC32().getText());
        A[2][2] = (home.getLC33().getText().contains("/")) ? (double) engine.eval(home.getLC33().getText()) : Double.parseDouble(home.getLC33().getText());

        return A;
    }

    public double[] getZeroDemand() throws ScriptException {

        double[] B = new double[3];

        B[0] = (home.getLCDemand1().getText().contains("/")) ? (double) engine.eval(home.getLCDemand1().getText()) : Double.parseDouble(home.getLCDemand1().getText());
        B[1] = (home.getLCDemand2().getText().contains("/")) ? (double) engine.eval(home.getLCDemand2().getText()) : Double.parseDouble(home.getLCDemand2().getText());
        B[2] = (home.getLCDemand3().getText().contains("/")) ? (double) engine.eval(home.getLCDemand3().getText()) : Double.parseDouble(home.getLCDemand3().getText());

        return B;
    }
}
