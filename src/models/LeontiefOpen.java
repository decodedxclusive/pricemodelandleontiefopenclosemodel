/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.stream.IntStream;
import javax.swing.JTextArea;
import static models.Inverse.invert;

/**
 *
 * @author the Code
 */
public class LeontiefOpen {

    public void InputValues(double[][] A, double[] B, JTextArea result) {

        result.setText("");

        int n = B.length;

        DecimalFormat dc = new DecimalFormat("0.000");

        double[][] I = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    I[i][j] = 1;
                } else {
                    I[i][j] = 0;
                }
            }
        }

        result.append("Matrices\n");

        result.append("I\n");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result.append(dc.format(I[i][j]) + "\t");
            }
            result.append("\n");
        }
        result.append("\n");

        result.append("A\n");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result.append(dc.format(A[i][j]) + "\t");
            }
            result.append("\n");
        }
        result.append("\n");

        result.append("(I - A)\n");

        double[][] IA = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                IA[i][j] = I[i][j] - A[i][j];
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result.append(dc.format(IA[i][j]) + "\t");
            }
            result.append("\n");
        }
        result.append("\n");

        double d[][] = invert(IA);

        result.append("The Inverse of (I - A): \n");
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                result.append(dc.format(d[i][j]) + "\t");
            }
            result.append("\n");
        }
        result.append("\n");

        result.append("Multiplying the matrices inv(I - A) * d \n");

        double[] C = Arrays.stream(d)
                .mapToDouble(row
                        -> IntStream.range(0, row.length)
                        .mapToDouble(col -> row[col] * B[col])
                        .sum()
                ).toArray();

        for (int i = 0; i < n; i++) {
            result.append(dc.format(C[i]) + "\n");
        }
    }
}
