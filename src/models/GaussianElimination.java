/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.text.DecimalFormat;
import javax.swing.JTextArea;

/**
 *
 * @author the Code
 */
public class GaussianElimination {

    /**
     * function to print in row echelon form *
     * @param A
     * @param B
     * @param result
     */
    public void solve(double[][] A, double[] B, JTextArea result) {

        int N = B.length;
        for (int k = 0; k < N; k++) {
            /**
             * find pivot row *
             */
            int max = k;
            for (int i = k + 1; i < N; i++) {
                if (Math.abs(A[i][k]) > Math.abs(A[max][k])) {
                    max = i;
                }
            }

            /**
             * swap row in A matrix *
             */
            double[] temp = A[k];
            A[k] = A[max];
            A[max] = temp;

            /**
             * swap corresponding values in constants matrix *
             */
            double t = B[k];
            B[k] = B[max];
            B[max] = t;

            /**
             * pivot within A and B *
             */
            for (int i = k + 1; i < N; i++) {
                double factor = A[i][k] / A[k][k];
                B[i] -= factor * B[k];
                for (int j = k; j < N; j++) {
                    A[i][j] -= factor * A[k][j];
                }
            }
        }

        /**
         * Print row echelon form *
         */
        printRowEchelonForm(A, B, result);
            
        /**
         * back substitution *
         */
        double[] solution = new double[N];
        for (int i = N - 1; i >= 0; i--) {
            double sum = 0.0;
            for (int j = i + 1; j < N; j++) {
                sum += A[i][j] * solution[j];
            }
            solution[i] = (B[i] - sum) / A[i][i];
        }
        /**
         * Print solution *
         */
        printSolution(solution, result);
    }

    /**
     * function to print in row echelon form *
     * @param A
     * @param B
     * @param result
     */
    public void printRowEchelonForm(double[][] A, double[] B, JTextArea result) {
        int N = B.length;
        DecimalFormat dc = new DecimalFormat("0.000");
        result.append("\nRow Echelon form : \n");
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                result.append(dc.format(A[i][j]) + "\t");
            }
            result.append("|  " + dc.format(B[i]) + "\n");
        }
        result.append("");
    }

    /**
     * function to print solution *
     * @param sol
     * @param result
     */
    public void printSolution(double[] sol, JTextArea result) {
        int N = sol.length;
        DecimalFormat dc = new DecimalFormat("0.000");
        result.append("\nSolution : \n");
        for (int i = 0; i < N; i++) {
            result.append(dc.format(sol[i]) + "\t");
        }
        result.append("");
    }

}
