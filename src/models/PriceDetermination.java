/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.text.DecimalFormat;
import javax.swing.JTextArea;

/**
 *
 * @author the Code
 */
public class PriceDetermination {

    public void InputValues(double[][] A, double[] B, JTextArea result) {
        result.setText("");
        
        int N = B.length;
        
        DecimalFormat dc = new DecimalFormat("0.000");

        result.append("Using Gaussian Elimination Algorithm\n");
        
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                result.append(dc.format(A[i][j]) + "\t");
            }
            result.append("|  " + dc.format(B[i]) + "\n");
        }
        result.append("\n");

        /**
         * Make an object of GaussianElimination class *
         */
        GaussianElimination ge = new GaussianElimination();

        ge.solve(A, B, result);

    }

}
